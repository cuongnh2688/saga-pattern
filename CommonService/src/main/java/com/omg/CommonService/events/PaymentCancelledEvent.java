package com.omg.CommonService.events;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PaymentCancelledEvent {

    @TargetAggregateIdentifier
    private String paymentId;

    private String orderId;

    private String paymentStatus;
}
