package com.omg.ShipmentService.command.api.event;

import com.omg.CommonService.events.OrderShippedEvent;
import com.omg.ShipmentService.command.api.data.Shipment;
import com.omg.ShipmentService.command.api.data.ShipmentRepository;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ShipmentsEventHandler {

    @Autowired
    private ShipmentRepository shipmentRepository;

    @EventHandler
    public void on(OrderShippedEvent event) {

        Shipment shipment = Shipment.builder()
                .shipmentId(event.getShipmentId())
                .orderId(event.getOrderId())
                .shipmentStatus(event.getShipmentStatus())
                .build();

        this.shipmentRepository.save(shipment);
    }
}
