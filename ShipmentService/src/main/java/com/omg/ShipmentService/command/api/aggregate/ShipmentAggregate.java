package com.omg.ShipmentService.command.api.aggregate;


import com.omg.CommonService.commands.ShipOrderCommand;
import com.omg.CommonService.events.OrderShippedEvent;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

@Aggregate
public class ShipmentAggregate {

    @AggregateIdentifier
    private String shipmentId;

    private String orderId;

    private String shipmentStatus;

    public ShipmentAggregate() {

    }


    @CommandHandler
    public ShipmentAggregate(ShipOrderCommand command) {

        // Validate the command
        // Publish the Order shipped event

        OrderShippedEvent event = OrderShippedEvent.builder()
                .shipmentStatus("COMPLETED")
                .shipmentId(command.getShipmentId())
                .orderId(command.getOrderId())
                .build();

        AggregateLifecycle.apply(event);
    }

    @EventSourcingHandler
    public void on(OrderShippedEvent event) {
        this.shipmentId = event.getShipmentId();
        this.orderId = event.getOrderId();
        this.shipmentStatus = event.getShipmentStatus();
    }
}
