package com.omg.UserService.projection;

import com.omg.CommonService.model.CardDetails;
import com.omg.CommonService.model.User;
import com.omg.CommonService.queries.GetUserPaymentDetailsQuery;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Component;

@Component
public class UserProjection {

    @QueryHandler
    public User getUserPaymentDetail (GetUserPaymentDetailsQuery query) {
        // Ideally get the details from the DB
        CardDetails cardDetails =
                CardDetails.builder()
                        .cardNumber("88 98874 38833 3773 53553")
                        .name("Kane Nguyen")
                        .validUntilMonth(2023)
                        .validUntilYear(10)
                        .cvv(111)
                        .build();

        return User.builder()
                .userId(query.getUserId())
                .firstName("Kane")
                .lastName("Nguyen")
                .cardDetails(cardDetails)
                .build();
    }
}
