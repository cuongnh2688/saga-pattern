package com.omg.OrderService.command.api.aggregate;

import com.omg.CommonService.commands.CancelOrderCommand;
import com.omg.CommonService.commands.CompleteOrderCommand;
import com.omg.CommonService.events.OrderCancelEvent;
import com.omg.CommonService.events.OrderCompletedEvent;
import com.omg.OrderService.command.api.command.CreatedOrderCommand;
import com.omg.OrderService.command.api.events.OrderCreatedEvent;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.beans.BeanUtils;

@Aggregate
public class OrderAggregate {

    @AggregateIdentifier
    private String orderId;

    private String productId;

    private String userId;

    private String addressId;

    private Integer quantity;

    private String orderStatus;

    public OrderAggregate() {

    }

    @CommandHandler
    public OrderAggregate(CreatedOrderCommand createdOrderCommand) {
        // Validate the command
        OrderCreatedEvent orderCreatedEvent = new OrderCreatedEvent();
        BeanUtils.copyProperties(createdOrderCommand, orderCreatedEvent);
        AggregateLifecycle.apply(orderCreatedEvent);
    }

    @EventSourcingHandler
    public void on(OrderCreatedEvent event) {
        this.orderStatus = event.getOrderStatus();
        this.orderId = event.getOrderId();
        this.addressId = event.getAddressId();
        this.productId = event.getProductId();
        this.quantity = event.getQuantity();
    }

    @CommandHandler
    public void handle(CompleteOrderCommand command) {

        // Validate command
        // Publish Order completed events
        OrderCompletedEvent event = OrderCompletedEvent.builder()
                .orderId(command.getOrderId())
                .orderStatus(command.getOrderStatus())
                .build();

        AggregateLifecycle.apply(event);
    }

    @EventSourcingHandler
    public void on(OrderCompletedEvent event) {
        this.orderStatus = event.getOrderStatus();
        this.orderId = event.getOrderId();
    }

    @CommandHandler
    public void handle(CancelOrderCommand command) {
        OrderCancelEvent event = new OrderCancelEvent();
        BeanUtils.copyProperties(command, event);
        AggregateLifecycle.apply(event);
    }

    @EventSourcingHandler
    public void on(OrderCancelEvent event) {
        this.orderStatus = event.getOrderStatus();
    }
}
