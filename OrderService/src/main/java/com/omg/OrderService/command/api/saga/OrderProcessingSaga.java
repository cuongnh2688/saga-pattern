package com.omg.OrderService.command.api.saga;

import com.omg.CommonService.commands.*;
import com.omg.CommonService.events.*;
import com.omg.CommonService.model.User;
import com.omg.CommonService.queries.GetUserPaymentDetailsQuery;
import com.omg.OrderService.command.api.events.OrderCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.modelling.saga.EndSaga;
import org.axonframework.modelling.saga.SagaEventHandler;
import org.axonframework.modelling.saga.StartSaga;
import org.axonframework.queryhandling.QueryGateway;
import org.axonframework.spring.stereotype.Saga;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

@Saga
@Slf4j
public class OrderProcessingSaga {

    @Autowired
    private transient CommandGateway commandGateway;

    @Autowired
    private transient QueryGateway queryGateway;

    @StartSaga
    @SagaEventHandler(associationProperty = "orderId")
    private void handle(OrderCreatedEvent event) {
        log.info("OrderCreatedEvent in Saga for Order ID: {}", event.getOrderId());

        GetUserPaymentDetailsQuery getUserPaymentDetailsQuery
                = new GetUserPaymentDetailsQuery(event.getUserId());

        User user = null;
        try {
            user = queryGateway.query(
                    getUserPaymentDetailsQuery,
                    ResponseTypes.instanceOf(User.class)
            ).join();
        } catch (Exception e) {
            log.error(e.getMessage());

            // Start the compensating transaction.
            this.cancelOrderCommand(event.getOrderId());
        }

        ValidatePaymentCommand validatePaymentCommand =
                ValidatePaymentCommand.builder()
                        .cardDetails(user.getCardDetails())
                        .orderId(event.getOrderId())
                        .paymentId(UUID.randomUUID().toString())
                        .build();

        this.commandGateway.sendAndWait(validatePaymentCommand);
    }

    private void cancelOrderCommand(String orderId) {

        CancelOrderCommand command = CancelOrderCommand.builder()
                .orderId(orderId)
                .orderStatus("CANCELLED")
                .build();

        this.commandGateway.sendAndWait(command);
    }

    @SagaEventHandler(associationProperty = "orderId")
    private void handle(PaymentProcessedEvent event) {

        log.info("PaymentProcessedEvent in saga for order id: {}", event.getOrderId());

        try {
            if(true) throw new Exception();
            ShipOrderCommand command =  ShipOrderCommand.builder()
                    .orderId(event.getOrderId())
                    .shipmentId(UUID.randomUUID().toString())
                    .build();

            this.commandGateway.sendAndWait(command);
        } catch (Exception e){
            log.error(e.getMessage());

            // Start the compensating transaction.
            this.cancelPaymentCommand(event);
        }
    }

    private void cancelPaymentCommand(PaymentProcessedEvent event) {

        CancelPaymentCommand cancelPaymentCommand = CancelPaymentCommand.builder()
                .paymentStatus("CANCELLED")
                .orderId(event.getOrderId())
                .paymentId(event.getPaymentId())
                .build();

        this.commandGateway.sendAndWait(cancelPaymentCommand);
    }

    @SagaEventHandler(associationProperty = "orderId")
    private void handle(OrderShippedEvent event) {
        log.info("OrderShippedEvent in saga for order id: {}", event.getOrderId());

        CompleteOrderCommand command = CompleteOrderCommand.builder()
                .orderId(event.getOrderId())
                .orderStatus("APPROVAL")
                .build();

        this.commandGateway.sendAndWait(command);
    }

    @SagaEventHandler(associationProperty = "orderId")
    @EndSaga
    public void handle(OrderCompletedEvent event) {
        log.info("OrderCompletedEvent in saga for order id: {}", event.getOrderId());
    }

    @SagaEventHandler(associationProperty = "orderId")
    @EndSaga
    public  void handle (OrderCancelEvent event) {
        log.info("OrderCancelEvent in saga for order id: {}", event.getOrderId());
    }

    @SagaEventHandler(associationProperty = "orderId")
    public void handle(PaymentCancelledEvent event) {
        log.info("PaymentCancelledEvent in saga for order id: {}", event.getOrderId());
        this.cancelOrderCommand(event.getOrderId());
    }
}
