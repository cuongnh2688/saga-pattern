package com.omg.OrderService.command.api.controller;

import com.omg.OrderService.command.api.command.CreatedOrderCommand;
import com.omg.OrderService.command.api.model.OrderRequest;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("api/orders")
public class OrderCommandController {

    @Autowired
    private CommandGateway commandGateway;

    @PostMapping
    public String createOrder(@RequestBody OrderRequest orderRequest) {
        String orderId = UUID.randomUUID().toString();
        CreatedOrderCommand createdOrderCommand =
                CreatedOrderCommand.builder()
                        .orderId(orderId)
                        .userId(orderRequest.getUserId())
                        .productId(orderRequest.getProductId())
                        .addressId(orderRequest.getAddressId())
                        .orderStatus("CREATED")
                        .build();
        commandGateway.sendAndWait(createdOrderCommand);
        return "Order Created";
    }
 }
