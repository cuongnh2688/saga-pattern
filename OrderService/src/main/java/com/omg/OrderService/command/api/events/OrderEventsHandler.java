package com.omg.OrderService.command.api.events;

import com.omg.CommonService.events.OrderCancelEvent;
import com.omg.CommonService.events.OrderCompletedEvent;
import com.omg.OrderService.command.api.data.Order;
import com.omg.OrderService.command.api.data.OrderRepository;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class OrderEventsHandler {

    @Autowired
    private OrderRepository orderRepository;

    @EventHandler
    public void on(OrderCreatedEvent event) {

        Order order = new Order();
        BeanUtils.copyProperties(event, order);
        this.orderRepository.save(order);
    }

    @EventHandler
    public void on(OrderCompletedEvent event) {

        Optional<Order> order = this.orderRepository.findById(event.getOrderId());
        Order data = order.get();
        data.setOrderStatus(event.getOrderStatus());
        this.orderRepository.save(data);
    }

    @EventHandler
    public void on (OrderCancelEvent event) {
        Optional<Order> order = this.orderRepository.findById(event.getOrderId());
        Order data = order.get();
        data.setOrderStatus(event.getOrderStatus());
        this.orderRepository.save(data);
    }
}
