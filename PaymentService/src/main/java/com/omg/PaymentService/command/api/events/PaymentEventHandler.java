package com.omg.PaymentService.command.api.events;

import com.omg.CommonService.events.PaymentCancelledEvent;
import com.omg.CommonService.events.PaymentProcessedEvent;
import com.omg.PaymentService.command.api.data.Payment;
import com.omg.PaymentService.command.api.data.PaymentRepository;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.text.html.Option;
import java.util.Date;
import java.util.Optional;

@Component
public class PaymentEventHandler {

    @Autowired
    private PaymentRepository paymentRepository;

    @EventHandler
    public void on(PaymentProcessedEvent event) {

        Payment payment = Payment.builder()
                .paymentId(event.getPaymentId())
                .orderId(event.getOrderId())
                .paymentStatus("COMPLETED")
                .timeStamp(new Date())
                .build();

        this.paymentRepository.save(payment);
    }

    @EventHandler
    public void on(PaymentCancelledEvent event) {
        Optional<Payment> payment = this.paymentRepository.findById(event.getPaymentId());

        Payment data = payment.get();
        data.setPaymentStatus(event.getPaymentStatus());

        this.paymentRepository.save(data);

    }
}
