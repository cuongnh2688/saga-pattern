package com.omg.PaymentService.command.api.aggregate;

import com.omg.CommonService.commands.CancelPaymentCommand;
import com.omg.CommonService.commands.ValidatePaymentCommand;
import com.omg.CommonService.events.PaymentCancelledEvent;
import com.omg.CommonService.events.PaymentProcessedEvent;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

@Aggregate
@Slf4j
public class PaymentAggregate {

    @AggregateIdentifier
    private String paymentId;

    private String orderId;

    private String paymentStatus;

    public PaymentAggregate() {

    }

    @CommandHandler
    public PaymentAggregate(ValidatePaymentCommand command) {

        // Validate the payment detail
        // Publish the Payment Processed event

        log.info("Executing ValidatePaymentCommand for OrderId:{} and PaymentId: {}",
                command.getOrderId(),
                command.getPaymentId());

        PaymentProcessedEvent event =
                new PaymentProcessedEvent( command.getPaymentId(), command.getOrderId());

        AggregateLifecycle.apply(event);

        log.info("PaymentProcessedEvent applied.");
    }

    @EventSourcingHandler
    public void on(PaymentProcessedEvent event) {
        this.paymentId = event.getPaymentId();
        this.orderId = event.getOrderId();
    }

    @CommandHandler
    public void handle(CancelPaymentCommand command) {

        PaymentCancelledEvent event = PaymentCancelledEvent.builder()
                .paymentStatus(command.getPaymentStatus())
                .paymentId(command.getPaymentId())
                .orderId(command.getOrderId())
                .build();

        AggregateLifecycle.apply(event);

        log.info("CancelPaymentCommand applied.");
    }

    @EventSourcingHandler
    public void on(PaymentCancelledEvent event) {
        this.paymentStatus = event.getPaymentStatus();
    }
}
